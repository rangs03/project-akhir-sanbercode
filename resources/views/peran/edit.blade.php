@extends('layout.master')
@section('title')
Halaman Edit Peran
@endsection

@section('content')

<form method="POST" action="/peran/{{$peran->id}}">
    @csrf
    @method('put')
    <div class="form-group">
        <label for="exampleInputnama">Nama Peran</label>
        <input type="text" name="nama" class="form-control" value="{{$peran->nama}}">
    <div class="form-group">

    @error('nama')
    <div class="alert alert-danger">{{$message}}</div>
    @enderror

    <div class="form-group">
        <label for="exampleInputfilm">Nama Film</label>
        <select name="film_id" class="form-control">
            <option value="">--pilih film--</option>
            @forelse ($film as $item)
            @if ($item->id === $peran->film_id)
            <option value="{{$item->id}}" selected>{{$item->judul}}</option>
                
            @else
            <option value="{{$item->id}}">{{$item->judul}}</option>
            @endif
            @empty
                <option value="">Tidak Ada Filmnya</option>
            @endforelse
        </select>
    <div class="form-group">

    @error('film')
    <div class="alert alert-danger">{{$message}}</div>
    @enderror

    <div class="form-group">
        <label for="exampleInputcast">Nama Cast</label>
        <select name="cast_id" class="form-control">
            <option value="">--pilih pemain--</option>
            @forelse ($cast as $pemain)
                <option value="{{$pemain->id}}">{{$pemain->nama}}</option>
            @empty
                <option value="">Tidak Ada Castnya</option>
            @endforelse
        </select>
    <div class="form-group">

    @error('cast')
    <div class="alert alert-danger">{{$message}}</div>
    @enderror

    <button type="submit" class="btn btn-primary">Submit</button>
</form>

@endsection

