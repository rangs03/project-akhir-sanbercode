@extends('layout.master')
@section('title')
Halaman Tambah Peran
@endsection

@section('content')

<form method="POST" action="/peran">
    @csrf
    <div class="form-group">
        <label for="exampleInputnama">Nama Peran</label>
        <input type="text" name="nama" class="form-control">
    <div class="form-group">

    @error('nama')
    <div class="alert alert-danger">{{$message}}</div>
    @enderror

    <div class="form-group">
        <label for="exampleInputfilm">Nama Film</label>
        <select name="film_id" class="form-control">
            <option value="">--pilih film--</option>
            @forelse ($Film as $item)
                <option value="{{$item->id}}">{{$item->judul}}</option>
            @empty
                <option value="">Tidak Ada Filmnya</option>
            @endforelse
        </select>
    <div class="form-group">

    @error('film')
    <div class="alert alert-danger">{{$message}}</div>
    @enderror

    <div class="form-group">
        <label for="exampleInputcast">Nama Cast</label>
        <select name="cast_id" class="form-control">
            <option value="">--pilih pemain--</option>
            @forelse ($cast as $pemain)
                <option value="{{$pemain->id}}">{{$pemain->nama}}</option>
            @empty
                <option value="">Tidak Ada Castnya</option>
            @endforelse
        </select>
    <div class="form-group">

    @error('cast')
        <div class="alert alert-danger">{{$message}}</div>
    @enderror
    </div>
    <button type="submit" class="btn btn-primary my-2">Submit</button>
</form>

@endsection

