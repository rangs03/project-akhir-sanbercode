@extends('layout.master')
@section('title')
Halaman Edit Profile
@endsection

@section('content')

@auth
    
<form method="POST" action="/profile/{{$profile->id}}">
    <div class="form-group">
        <label for="">Nama</label>
        <input type="text" disabled value="{{$profile->user->name}}" class="form-control">
    </div>
    <div class="form-group">
        <label for="">Email</label>
        <input type="text" disabled value="{{$profile->user->email}}" class="form-control">
    </div>
    @csrf
    @method('put')
    <div class="form-group">
        <label for="exampleInputUmur">Umur</label>
        <input type="number" name="umur" class="form-control" value="{{$profile->umur}}">
    </div>
    @error('umur')
        <div class="alert alert-danger">{{$message}}</div>
    @enderror
    <div class="form-group">
        <label for="exampleInputBio">Biodata</label>
        <textarea name="bio" class="form-control" cols="30" rows="10">{{$profile->bio}}</textarea>
    </div>
    @error('bio')
        <div class="alert alert-danger">{{$message}}</div>
    @enderror
    <div class="form-group">
        <label for="exampleInputAlamat">Alamat</label>
        <textarea name="alamat" class="form-control" cols="30" rows="10">{{$profile->alamat}}</textarea>
    </div>
    @error('alamat')
        <div class="alert alert-danger">{{$message}}</div>
    @enderror
    <button type="submit" class="btn btn-primary">Submit</button>
</form>
@endauth

@endsection