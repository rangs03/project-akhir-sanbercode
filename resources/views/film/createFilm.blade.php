@extends('layout.master')
@section('title')
Halaman Tambah Film
@endsection

@section('content')

<form method="POST" enctype="multipart/form-data" action="/film">
    @csrf
    <div class="form-group">
        <label for="exampleInputjudul">Judul</label>
        <input type="text" name="judul" class="form-control">
    <div>
    @error('judul')
        <div class="alert alert-danger">{{$message}}</div>
    @enderror
    <div class="form-group">
        <label for="exampleInputRingkasan">Ringkasan</label>
        <textarea name="ringkasan" class="form-control" cols="30" rows="10"></textarea>
    </div>
    @error('ringkasan')
        <div class="alert alert-danger">{{$message}}</div>
    @enderror
    <div class="form-group">
        <label for="exampleInputTahun">Tahun</label>
        <input type="number" name="tahun" class="form-control">
    </div>
    @error('tahun')
        <div class="alert alert-danger">{{$message}}</div>
    @enderror
    <div class="form-group">
        <label for="exampleInputPoster">Poster</label>
        <input type="file" name="poster" class="form-control">
    <div class="form-group">
    @error('poster')
        <div class="alert alert-danger">{{$message}}</div>
    @enderror
    <div class="form-group">
        <label for="exampleInputGenre">Genre</label>
        <select class="form-control" name="genre_id" id="genre_id">
            <option value="">--Pilih Genre--</option>
            @forelse ($genre as $item)
                <option value="{{$item->id}}">{{$item->nama}}</option>
            @empty
                <option value="">Belum ada data kategori</option>
            @endforelse
        </select>
    </div>
    @error('genre_id')
        <div class="alert alert-danger">{{$message}}</div>
    @enderror
    <button type="submit" class="btn btn-primary">Submit</button>
</form>

@endsection