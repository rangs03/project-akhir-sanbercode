@extends('layout.master')
@section('title')
Halaman Detail Film
@endsection

@push('scripts')
<script src="https://cdn.tiny.cloud/1/eef2qjm0o7psa62u1isj79k1r3uu1w8lz2vwz0rlbruxt5w4/tinymce/6/tinymce.min.js" referrerpolicy="origin"></script>
<script>
  tinymce.init({
    selector: 'textarea',
    plugins: 'anchor autolink charmap codesample emoticons image link lists media searchreplace table visualblocks wordcount checklist mediaembed casechange export formatpainter pageembed linkchecker a11ychecker tinymcespellchecker permanentpen powerpaste advtable advcode editimage tinycomments tableofcontents footnotes mergetags autocorrect typography inlinecss',
    toolbar: 'undo redo | blocks fontfamily fontsize | bold italic underline strikethrough | link image media table mergetags | addcomment showcomments | spellcheckdialog a11ycheck typography | align lineheight | checklist numlist bullist indent outdent | emoticons charmap | removeformat',
    tinycomments_mode: 'embedded',
    tinycomments_author: 'Author name',
    mergetags_list: [
      { value: 'First.Name', title: 'First Name' },
      { value: 'Email', title: 'Email' },
    ]
  });
</script>
@endpush

@section('content')

<div class="card">
    <img class="card-img-top" src="{{asset('image/'.$film->poster)}}" alt="Card image cap">
    <div class="card-body">
      <h2>{{$film->judul}}</h2>
      <h2>Tayang pada tahun : {{$film->tahun}}</h2>
      <p class="card-text">{{$film->ringkasan}}</p>

      <hr>
      <h4>
        List Kritik

        @forelse ($film->kritik as $item)
          <div class="card my-4">
            <div class="card-header">
              {{$item->user->name}}
            </div>
            <div class="card-body">
              <p class="card-text">{!!$item->content!!}</p>            
            </div>
            <div class="card-footer">
              <p class="card-text">Nilai Film : {{$item->point}}</p>            
            </div>
          </div>
        @empty
            <h6>Belum ada kritik saat ini.</h6>
        @endforelse

      </h4>
      @auth
          <form action="/kritik/{{$film->id}}" method="post">
          @csrf
          <textarea name="content" id="" cols="30" rows="10" class="form-control" placeholder="isi kritik"></textarea>

          @error('content')
          <div class="alert alert-danger">{{$message}}</div>
          @enderror
          <p>Beri Rating pada Film ini antara 1 - 100</p>
          <input type="number" name="point" id="">
          @error('point')
          <div class="alert alert-danger">{{$message}}</div>
          @enderror
          <br>
          <input type="submit" value="kirim" class="btn btn-primary btn-sm my-2">
          </form>
      @endauth
      <hr>
    </div>
    <a href="/film" class="btn btn-block btn-success my-2 ">Kembali</a>
</div>

@endsection