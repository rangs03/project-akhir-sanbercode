@extends('layout.master')
@section('title')
Halaman Film
@endsection

@section('content')
@auth
<a href="/film/create" class="btn btn-primary btn-sm my-2">Tambah</a>
@endauth

<div class="row">
    @forelse ($film as $item)
    <div class="col-4">
        <div class="card">
            <img class="card-img-top" src="{{asset('image/'.$item->poster)}}" alt="Card image cap">
            <div class="card-body">
              <h2>{{$item->judul}} ({{$item->tahun}})</h2>
              <p class="card-text">{{ Str::limit($item->ringkasan, 50)}}</p>
              <a href="/film/{{$item->id}}" class="btn btn-block btn-info my-2 ">Detail</a>
              @auth
              <div>
                  <form action="/film/{{$item->id}}" method="POST">
                      <div class="row">
                          <div class="col">
                              <a href="/film/{{$item->id}}/edit" class="btn btn-block btn-warning btn-sm">Edit</a>
                          </div>
                          <div class="col">
                              @csrf
                              @method('delete')
                              <input type="submit" value="Delete" class="btn btn-block btn-danger btn-sm">
                          </div>
                      </div>
                  </form>
                </div>
                @endauth
            </div>
        </div>
    </div>
    @empty
        <h1>Belum Ada Daftar Film</h1>
    @endforelse
</div>

@endsection