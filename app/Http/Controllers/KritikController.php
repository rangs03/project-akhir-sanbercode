<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\kritik;
use Illuminate\Support\Facades\Auth;
use RealRashid\SweetAlert\Facades\Alert;


class KritikController extends Controller
{
    public function store(Request $request,$id)
    {
        $request -> validate([
            'content' => 'required'
        ]);

        $idUser = Auth::id();

        $kritik = new kritik;
        $kritik->content = $request['content'];
        $kritik->point = $request['point'];
        $kritik->film_id = $id;
        $kritik->user_id = $idUser;
        
        $kritik->save();

        Alert::success('Berhasil', 'Berhasil Menambah Kritik');

        return redirect('/film/' . $id);

    }
}
