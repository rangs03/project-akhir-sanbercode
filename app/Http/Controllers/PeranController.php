<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Models\Film;
use App\Models\Cast;
use App\Models\peran;
use RealRashid\SweetAlert\Facades\Alert;

class PeranController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $peran = peran::get();
        return view('peran.index',['peran'=>$peran]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $film = Film::all();
        $cast = Cast::all();
        return view('peran.createPeran',['Film'=>$film],['cast'=>$cast]);
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'nama' => 'required'
        ]);

        $peran = new peran;

        $peran->nama = $request['nama'];
        $peran->cast_id = $request['cast_id'];
        $peran->film_id = $request['film_id'];

        $peran->save();

        Alert::success('Berhasil', 'Berhasil Menambah Peran');

        return redirect('/peran');
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $peran = peran::find($id);

        return view('peran.detail',['peran'=>$peran]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $peran = peran::find($id);
        $film = Film::get();
        $cast = Cast::get();
        
        return view('peran.edit',['peran'=>$peran,'film'=>$film,'cast'=>$cast]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'nama' => 'required'
        ]);

        $peran = peran::find($id);

        $peran->nama = $request->nama;
        $peran->film_id = $request->film_id;
        $peran->cast_id = $request->cast_id;

        $peran->save();
        
        Alert::success('Berhasil', 'Berhasil Mengedit Peran');

        return redirect('/peran');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $peran = peran::find($id);

        $peran->delete();

        Alert::success('Berhasil', 'Berhasil Menghapus Peran');

        return redirect('/peran');
    }
}
