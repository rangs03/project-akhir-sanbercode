<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use PhpParser\Node\Expr\Cast;
use RealRashid\SweetAlert\Facades\Alert;


class CastController extends Controller
{
    public function index(){
        $cast = DB::table('cast')->get();
        return view('cast.index', ['cast' => $cast]);
    }

    public function create(){
        return view('/cast.createCast');
    }

    public function store(Request $request){

        // validasi data
        $request->validate([
            'nama' => 'required|min:4|max:255|',
            'umur' => 'required|numeric|min:1',
            'bio' => 'required|min:10'
        ]);

        // masukan data request ke db table cast
        DB::table('cast')->insert([
            'nama' => $request['nama'],
            'umur' => $request['umur'],
            'bio' => $request['bio']
        ]);

        Alert::success('Berhasil', 'Berhasil Menambah Cast');

        // lempar ke halaman /cast
        return redirect('/cast');
    }

    public function show($id){
        $cast = DB::table('cast')->find($id);

        return view('cast.detail', ['cast' => $cast]);
    }

    public function edit($id){
        $cast = DB::table('cast')->find($id);

        return view('cast.edit', ['cast' => $cast]);
    }

    public function update($id, Request $request){
        // validasi data
        $request->validate([
            'nama' => 'required|min:4|max:255|',
            'umur' => 'required|numeric|min:1',
            'bio' => 'required|min:10'
        ]);

        // update data table
        DB::table('cast')
            ->where('id', $id)
            ->update(
            [
                'nama' => $request['nama'],
                'umur' => $request['umur'],
                'bio' => $request['bio']
            ]
        );

        Alert::success('Berhasil', 'Berhasil Mengedit Cast');

        // lempar ke halaman /cast
        return redirect('/cast');
    }

    public function destroy($id){

        // delete data table
        DB::table('cast')->where('id', $id)->delete();

        Alert::success('Berhasil', 'Berhasil Menghapus Cast');

        return redirect('/cast');
    }
}
