<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Film extends Model
{
    use HasFactory;

    protected $table = 'film';

    protected $fillable = ['judul', 'ringkasan', 'tahun', 'poster', 'genre_id'];

    public function peran()
    {
        return $this->hasMany(peran::class,'film_id');
    }

    public function kritik()
    {
        return $this->hasMany(kritik::class,'film_id');
    }

}
